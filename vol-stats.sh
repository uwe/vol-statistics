#!/bin/bash

shopt -s expand_aliases

#assume linux and fix commands if its not the case

command -v md5sum >/dev/null 2>&1 || alias portable_md5sum='md5 -r'
command -v md5sum >/dev/null 2>&1 && alias portable_md5sum='md5sum'
stat --printf %h\  . >/dev/null 2>&1 || alias portable_stat='stat -n -f %l\ '
stat --printf %h\  . >/dev/null 2>&1 && alias portable_stat='stat --printf %h\ '

command -v md5sum >/dev/null 2>&1 || alias md5sum = 'md5 -r'
stat --printf %h\  . >/dev/null 2>&1 || alias "stat --printf %h\ "='stat -n -f %l\ '

RUNID=$RANDOM
if [[ $# -ne 1 ]]
then 
    echo "Please enter Volume path: (leave empty and hit enter for help; CTRL+z to terminate)"
    read volPath
    if [[ -z "$volPath" ]]
    then
        echo "ERROR: Valid path needed as first argumetn"
        echo "Syntax: $0 PATH"
        echo "This script creates a gzipped tar archive that includes a directory structure and md5sum hash of files under PATH (only argument passed to script)"
        exit 1
    fi
else
    volPath=$1
fi

if [[ -d "$volPath" ]]
then
	mkdir /tmp/vol-stats-$RUNID
	cd "$volPath" && \
	pwd > /tmp/vol-stats-$RUNID/pwd
	du -khs . > /tmp/vol-stats-$RUNID/disk-usage && \
	find . -type d > /tmp/vol-stats-$RUNID/paths && \
	#find . -type f -exec stat -n -f %l\  {} \; -exec md5 -r {} \; > /tmp/vol-stats-$RUNID/links-md5sums && \
	#find . -type f -exec stat -n -f %l\  {} \; -exec md5sum {} \; > /tmp/vol-stats-$RUNID/links-md5sums && \
	#find . -type f -exec stat --printf %h\  {} \; -exec md5sum {} \; > /tmp/vol-stats-$RUNID/links-md5sums && \
	find . -type f -exec ${BASH_ALIASES[portable_stat]} {} \; -exec ${BASH_ALIASES[portable_md5sum]} {} \; > /tmp/vol-stats-$RUNID/links-md5sums && \
	find . \! -type f \! -type d > /tmp/vol-stats-$RUNID/other && \
	tar czf /tmp/vol-stats-$RUNID.tgz -C /tmp/vol-stats-$RUNID/ ./  && \
	echo Results: /tmp/vol-stats-$RUNID.tgz || echo "ERROR: Something did not run properly, check that you are running the script with sufficient read permission on volume path, partial results can be found in /tmp/vol-stats-$RUNID.tgz"

else
    echo "ERROR: Valid path needed as first argument"
    exit 2
fi
